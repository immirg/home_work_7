//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {
        Main main = new Main();
        main.printCar();
    }

    public void printCar(){
        Car car = new Car();
        System.out.println("Car " + car.manufacturer);
        System.out.println("Color " + car.color);
        car.startTestCar();
        car.stopTestCar();
        car.newPrice();
        car.suppliersList();

        Shops shops = new Shops();
        shops.printShops();
    }
}
