public class Car {
    public String manufacturer;
    public String color;

    public Car(String manufacturer, String color){
        // конструктор с параметрами
        this.manufacturer = manufacturer;
        this.color = color;
    }
    public Car(){
        //конструктор без параметров
        this.manufacturer = "Fiat";
        this.color = "Red";
    }
    private void setModel(String model){
        this.manufacturer = model;
    }
    public void newModel(String newCar){
        setModel(newCar);
    }
    private void setColor(String color){
        this.color = color;
    }
    public String getModel(){
        return manufacturer;
    }
    public String getColor(){
        return color;
    }

    public void startTestCar(){
        String startTest = "Start test car";
        System.out.println(startTest);
    }
    public void stopTestCar(){
        System.out.println("Stop test car");
    }
    public void newPrice(){
        Price price = new Price();
        System.out.println("New price: " + price.calculateFinalPrice());
    }
    public void suppliersList(){
        Suppliers suppliers = new Suppliers();
        suppliers.printSuppliers();
    }
    private class Price {
        int discount;
        int price;

        public Price(int discount, int price){
            // конструктор с параметрами
            this.discount = discount;
            this.price = price;
        }
        public Price(){
            //конструктор без параметров
            this.discount = 5;
            this.price = 100;
        }
        private void setDiscount(int discount){
            this.discount = discount;
        }
        private void setPrice(int price){
            this.price = price;
        }
        public int getDiscount(){
            return discount;
        }
        public int getPrice(){
            return price;
        }
        public int calculateFinalPrice(){
            return price - discount;
        }
    }
    protected class Suppliers{
        String supplier1;
        String supplier2;
        public Suppliers(String supplier1, String supplier2){
            // конструктов с параметрами
            this.supplier1 = supplier1;
            this.supplier2 = supplier2;
        }
        public Suppliers(){
            //конструктор без параметров
            this.supplier1 = "Supplier1";
            this.supplier2 = "Supplier2";
        }
        private void setSupplier1(String supplier1){
            this.supplier1 = supplier1;
        }
        private void setSupplier2(String supplier2){
            this.supplier2 = supplier2;
        }
        public String getSupplier1(){
            return supplier1;
        }
        public String getSupplier2(){
            return supplier2;
        }
        public void printSuppliers(){
            System.out.println("Supplier 1: " + supplier1);
            System.out.println("Supplier 2: " + supplier2);
        }
    }
}

class Shops {
    protected String shop1;
    protected String shop2;

    public Shops(String shop1, String shop2){
        //конструктор с параметрами
        this.shop1 = shop1;
        this.shop2 = shop2;
    }
    public Shops(){
        //конструктор без параметров
        this.shop1 = "Shop1";
        this.shop2 = "Shop2";
    }
    private void setShop1(String shop1){
        this.shop1 = shop1;
    }
    private void setShop2(String shop2){
        this.shop2 = shop2;
    }
    public String getShop1(){
        return shop1;
    }
    public String getShop2(){
        return shop2;
    }
    public void printShops(){
        System.out.println("Shop 1: " + shop1);
        System.out.println("Shop 2: " + shop2);
    }
}
